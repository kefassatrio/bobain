package kelompok2.kece.Bobain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BobainApplication {

	public static void main(String[] args) {
		SpringApplication.run(BobainApplication.class, args);
	}

}
