package kelompok2.kece.Bobain.repository;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.CoR.*;
import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.ConcreteBoba.MilkBoba;
import kelompok2.kece.Bobain.model.ConcreteBoba.TeaBoba;
import kelompok2.kece.Bobain.model.Factory.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Repository
public class BobaRepository {
    private Map<UUID, Boba> allBoba = new HashMap<>();
    private HashMap<String, String> selectedToppings = new HashMap<>();
    private int selectedToppingsCost = 0;
    private Boba currentBoba = new ClearBoba(UUID.randomUUID());
    private BobaFactory bobaFactory;

    public Boba getCurrentBoba(){
        return this.currentBoba;
    }

    public Boba setCurrentBoba(String bobaType){
        UUID id = currentBoba.getId();
        if(bobaType.equalsIgnoreCase("clear")){
            currentBoba = new ClearBoba(id);
        }
        else if(bobaType.equalsIgnoreCase("tea")){
            currentBoba = new TeaBoba(id);
        }
        else if(bobaType.equalsIgnoreCase("milk")){
            currentBoba = new MilkBoba(id);
        }
        return currentBoba;
    }

    public void saveCurrentBoba(int jumlahBoba){
        for(int i=0; i<jumlahBoba; i++){
            Boba savedBoba = currentBoba.copyOfConcreteBoba().addToppings(new ArrayList<>(selectedToppings.values()));
            save(savedBoba);
        }
        selectedToppings = new HashMap<>();
        selectedToppingsCost = 0;
        currentBoba = new ClearBoba(UUID.randomUUID());
    }

    public Boba save(Boba savedBoba) {
        allBoba.put(savedBoba.getId(), savedBoba);
        return allBoba.get(savedBoba.getId());
    }

    public Boba getBobaById(UUID id){
        return allBoba.get(id);
    }

    public ArrayList<Boba>getAllBoba(){
        return new ArrayList<>(allBoba.values());
    }

    public HashMap<String, String> getSelectedToppings(){
        return this.selectedToppings;
    }

    public void addTopping(String topping){
        if(selectedToppings.size()<3 && !selectedToppings.containsKey(topping)){
            String formattedTopping = formattedString(topping);
            if(formattedTopping.equalsIgnoreCase("Ice") || formattedTopping.equalsIgnoreCase("Cheese")
                    || formattedTopping.equalsIgnoreCase("Brown Sugar") || formattedTopping.equalsIgnoreCase("Black Bubble")) {
                selectedToppings.put(topping, formattedTopping);
                addSelectedToppingsCost(formattedTopping);
            }
        }
    }

    public void removeTopping(String topping){
        if(selectedToppings.containsKey(topping)){
            selectedToppings.remove(topping);
            String formattedTopping = formattedString(topping);
            removeSelectedToppingsCost(formattedTopping);
        }
    }

    public void addSelectedToppingsCost(String topping){
        if(topping.equalsIgnoreCase("Ice"))
            this.selectedToppingsCost += 3000;
        else if(topping.equalsIgnoreCase("Cheese") ||
                topping.equalsIgnoreCase("Brown Sugar") ||
                topping.equalsIgnoreCase("Black Bubble"))
            this.selectedToppingsCost += 5000;
    }

    public void removeSelectedToppingsCost(String topping){
        if(topping.equalsIgnoreCase("Ice"))
            this.selectedToppingsCost -= 3000;
        else if(topping.equalsIgnoreCase("Cheese") ||
                topping.equalsIgnoreCase("Brown Sugar") ||
                topping.equalsIgnoreCase("Black Bubble"))
            this.selectedToppingsCost -= 5000;
    }

    public String getCurrentBobaType(){
        return this.currentBoba.getType();
    }

    public int getSelectedToppingsCost(){
        return this.selectedToppingsCost;
    }

    public void clearRepo(){
        allBoba = new HashMap<>();
        selectedToppings = new HashMap<>();
        currentBoba = new ClearBoba(UUID.randomUUID());
    }

    public String formattedString(String line){
        line = line.replace("-", " ");
        String[] lineArr = line.split(" ");
        for(int i = 0; i<lineArr.length; i++){
            lineArr[i] = capitalize(lineArr[i]);
        }
        line = String.join(" ", lineArr);
        return line;
    }

    public String capitalize(String line){
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    public ArrayList<String> getAllToppings(){
        ArrayList<String> listToppings = new ArrayList<String>();
        for(int i = 0; i < this.getAllBoba().size(); i++){
            for(int j = 0; j < this.getAllBoba().get(i).getToppings().size(); j++){
                listToppings.add(this.getAllBoba().get(i).getToppings().get(j));
            }
        }
        return listToppings;
    }

    public ArrayList<String> getDaftarToko(){
        ArrayList<String> listToppings = getAllToppings();
        HandlerAttribute bobainHandler = new BobainHandler();
        Handler chatimeHandler = new ChatimeHandler();
        Handler hophopHandler = new HophopHandler();
        Handler koiHandler = new KoiHandler();
        Handler xingfutangHandler = new XingfutangHandler();
        bobainHandler.setNextHandler(chatimeHandler);
        chatimeHandler.setNextHandler(hophopHandler);
        hophopHandler.setNextHandler(koiHandler);
        koiHandler.setNextHandler(xingfutangHandler);
        bobainHandler.handle(listToppings, bobainHandler.daftarToko);
        return bobainHandler.daftarToko;
    }

    public void saveComboBoba(Boba comboBoba){
        save(comboBoba);
        selectedToppings = new HashMap<>();
        selectedToppingsCost = 0;
        this.currentBoba = new ClearBoba(UUID.randomUUID());
    }

}
