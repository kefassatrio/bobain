package kelompok2.kece.Bobain.model.ConcreteBoba;

import kelompok2.kece.Bobain.model.Boba;

import java.util.UUID;

public class TeaBoba extends Boba {

    public TeaBoba(UUID id){
        this.id = id;
        this.description = "Tea Boba";
        this.type = "Tea";
    }

    @Override
    public int getCost() {
        return 15000;
    }
}
