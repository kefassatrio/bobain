package kelompok2.kece.Bobain.model.ConcreteBoba;

import kelompok2.kece.Bobain.model.Boba;

import java.util.UUID;

public class MilkBoba extends Boba {

    public MilkBoba(UUID id){
        this.id = id;
        this.description = "Milk Boba";
        this.type = "Milk";
    }

    @Override
    public int getCost() {
        return 17000;
    }
}
