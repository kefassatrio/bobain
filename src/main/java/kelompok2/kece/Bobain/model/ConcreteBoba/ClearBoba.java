package kelompok2.kece.Bobain.model.ConcreteBoba;

import kelompok2.kece.Bobain.model.Boba;

import java.util.UUID;

public class ClearBoba extends Boba {

    public ClearBoba(UUID id){
        this.id = id;
        this.description = "Clear Boba";
        this.type = "Clear";
    }

    @Override
    public int getCost() {
        return 10000;
    }
}
