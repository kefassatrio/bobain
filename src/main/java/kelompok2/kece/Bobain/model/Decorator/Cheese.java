package kelompok2.kece.Bobain.model.Decorator;

import kelompok2.kece.Bobain.model.Boba;

public class Cheese extends Decorator {

    public Cheese(Boba boba){
        this.boba = boba;
        this.boba.getToppings().add("Cheese");
    }

    @Override
    public String getDescription() {
        if(isConcreteBoba()){
            return this.boba.getDescription() + " with Cheese";
        }
        return this.boba.getDescription() + ", Cheese";
    }

    @Override
    public int getCost() {
        return this.boba.getCost() + 5000;
    }
}
