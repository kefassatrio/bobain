package kelompok2.kece.Bobain.model.Decorator;

import kelompok2.kece.Bobain.model.Boba;

import java.util.ArrayList;
import java.util.UUID;

public abstract class Decorator extends Boba {
    Boba boba;

    public abstract String getDescription();

    public boolean isConcreteBoba(){
        String[] desc = this.boba.getDescription().split(" ");
        String lastDesc = desc[desc.length-1];
        return lastDesc.equals("Boba");
    }

    @Override
    public UUID getId(){
        return this.boba.getId();
    }

    @Override
    public ArrayList<String> getToppings(){
        return this.boba.getToppings();
    }

    @Override
    public String getType(){
        return this.boba.getType();
    }
}
