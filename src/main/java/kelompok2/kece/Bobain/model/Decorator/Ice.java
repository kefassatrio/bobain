package kelompok2.kece.Bobain.model.Decorator;

import kelompok2.kece.Bobain.model.Boba;

public class Ice extends Decorator {

    public Ice(Boba boba){
        this.boba = boba;
        this.boba.getToppings().add("Ice");
    }

    @Override
    public String getDescription() {
        if(isConcreteBoba()){
           return this.boba.getDescription() + " with Ice";
        }
        return this.boba.getDescription() + ", Ice";
    }

    @Override
    public int getCost() {
        return this.boba.getCost() + 3000;
    }
}
