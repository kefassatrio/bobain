package kelompok2.kece.Bobain.model.Decorator;

import kelompok2.kece.Bobain.model.Boba;

public class BlackBubble extends Decorator {

    public BlackBubble(Boba boba){
        this.boba = boba;
        this.boba.getToppings().add("Black Bubble");
    }

    @Override
    public String getDescription() {
        if(isConcreteBoba()){
            return this.boba.getDescription() + " with Black Bubble";
        }
        return this.boba.getDescription() + ", Black Bubble";
    }

    @Override
    public int getCost() {
        return this.boba.getCost() + 5000;
    }
}
