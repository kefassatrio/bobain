package kelompok2.kece.Bobain.model.Decorator;

import kelompok2.kece.Bobain.model.Boba;

public class BrownSugar extends Decorator {

    public BrownSugar(Boba boba){
        this.boba = boba;
        this.boba.getToppings().add("Brown Sugar");
    }

    @Override
    public String getDescription() {
        if(isConcreteBoba()){
            return this.boba.getDescription() + " with Brown Sugar";
        }
        return this.boba.getDescription() + ", Brown Sugar";
    }

    @Override
    public int getCost() {
        return this.boba.getCost() + 5000;
    }
}
