package kelompok2.kece.Bobain.model.Factory;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.Decorator.BlackBubble;
import kelompok2.kece.Bobain.model.Decorator.Cheese;

import java.util.ArrayList;
import java.util.UUID;

public class CheesyBubbleFactory implements BobaFactory{

    private ArrayList<String> toppingList = new ArrayList<>();

    @Override
    public Boba produceBoba() {
        Boba base = new ClearBoba(UUID.randomUUID());
        setToppingList();
        Boba cheesyBoba = base.addToppings(toppingList);
        return cheesyBoba;
    }

    @Override
    public String getTitle() {
        return "Cheesy Bubble";
    }

    public void setToppingList(){
        toppingList.add("Black Bubble");
        toppingList.add("Cheese");
    }
}
