package kelompok2.kece.Bobain.model.Factory;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.TeaBoba;
import kelompok2.kece.Bobain.model.Decorator.BlackBubble;
import kelompok2.kece.Bobain.model.Decorator.Ice;

import java.util.ArrayList;
import java.util.UUID;

public class IceBubbleTeaFactory implements BobaFactory{

    private ArrayList<String> toppingList = new ArrayList<>();

    @Override
    public Boba produceBoba() {
        Boba base = new TeaBoba(UUID.randomUUID());
        setToppingList();
        Boba iceBubbleTea = base.addToppings(toppingList);
        return iceBubbleTea;
    }

    @Override
    public String getTitle() {
        return "Ice Bubble Tea";
    }

    public void setToppingList(){
        toppingList.add("Black Bubble");
        toppingList.add("Ice");
    }
}