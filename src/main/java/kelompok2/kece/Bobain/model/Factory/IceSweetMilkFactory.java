package kelompok2.kece.Bobain.model.Factory;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.MilkBoba;
import kelompok2.kece.Bobain.model.Decorator.BlackBubble;
import kelompok2.kece.Bobain.model.Decorator.BrownSugar;

import java.util.ArrayList;
import java.util.UUID;

public class IceSweetMilkFactory implements BobaFactory{

    private ArrayList<String> toppingList = new ArrayList<>();

    @Override
    public Boba produceBoba() {
        Boba base = new MilkBoba(UUID.randomUUID());
        setToppingList();
        Boba iceSweetMilk = base.addToppings(toppingList);
        return iceSweetMilk;
    }

    @Override
    public String getTitle() {
        return "Ice Sweet Milk";
    }

    public void setToppingList(){
        toppingList.add("Brown Sugar");
        toppingList.add("Ice");
    }
}
