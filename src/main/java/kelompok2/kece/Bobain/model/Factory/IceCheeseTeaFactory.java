package kelompok2.kece.Bobain.model.Factory;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.TeaBoba;
import kelompok2.kece.Bobain.model.Decorator.Cheese;
import kelompok2.kece.Bobain.model.Decorator.Ice;

import java.util.ArrayList;
import java.util.UUID;

public class IceCheeseTeaFactory implements BobaFactory{

    private ArrayList<String> toppingList = new ArrayList<>();

    @Override
    public Boba produceBoba() {
        Boba base = new TeaBoba(UUID.randomUUID());
        setToppingList();
        Boba iceCheeseTea = base.addToppings(toppingList);
        return iceCheeseTea;
    }

    @Override
    public String getTitle() {
        return "Ice Cheese Tea";
    }

    public void setToppingList(){
        toppingList.add("Cheese");
        toppingList.add("Ice");
    }

}
