package kelompok2.kece.Bobain.model.Factory;

import kelompok2.kece.Bobain.model.Boba;

public interface BobaFactory {
    Boba produceBoba();
    String getTitle();
}
