package kelompok2.kece.Bobain.model.CoR;

import java.util.ArrayList;

public class XingfutangHandler implements Handler {
    private Handler nextHandler;

    @Override
    public void setNextHandler(Handler nextHandler) { this.nextHandler = nextHandler; }

    @Override
    public void handle(ArrayList<String> toppings, ArrayList<String> daftarToko) {
        this.nextHandler = nextHandler;
            if (!toppings.contains("Cheese")) {
                daftarToko.add("Xing Fu Tang Boba Store");
            }
    }

    @Override
    public Handler getNextHandler() {
        return this.nextHandler;
    }
}