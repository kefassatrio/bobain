package kelompok2.kece.Bobain.model.CoR;

import java.util.ArrayList;

public class HophopHandler implements Handler {
    private Handler nextHandler;

    @Override
    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    @Override
    public void handle(ArrayList<String> toppings, ArrayList<String> daftarToko) {
        this.nextHandler = nextHandler;
        if(this.nextHandler != null) {
            if (!toppings.contains("Black Bubble")) {
                daftarToko.add("Hop Hop Boba Store");
            }
            this.nextHandler.handle(toppings, daftarToko);
        }
    }

    @Override
    public Handler getNextHandler() {
        return this.nextHandler;
    }
}