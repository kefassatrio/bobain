package kelompok2.kece.Bobain.model.CoR;
import java.util.ArrayList;

public interface Handler {
    public abstract void setNextHandler(Handler nextHandler);
    public abstract void handle(ArrayList<String> toppings, ArrayList<String> daftarToko);
    public abstract Handler getNextHandler();
}