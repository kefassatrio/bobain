package kelompok2.kece.Bobain.model;

import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.ConcreteBoba.MilkBoba;
import kelompok2.kece.Bobain.model.ConcreteBoba.TeaBoba;
import kelompok2.kece.Bobain.model.Decorator.BlackBubble;
import kelompok2.kece.Bobain.model.Decorator.BrownSugar;
import kelompok2.kece.Bobain.model.Decorator.Cheese;
import kelompok2.kece.Bobain.model.Decorator.Ice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.UUID;

public abstract class Boba {
    protected UUID id;
    protected String type;
    protected String description;
    protected ArrayList<String> toppings = new ArrayList<>();

    public String getDescription(){
        return description;
    }

    public abstract int getCost();

    public UUID getId(){
        return this.id;
    }

    public void setId(UUID id){
        this.id = id;
    }

    public String getType(){ return this.type; }

    public ArrayList<String> getToppings() {
        return this.toppings;
    }

    public double getDiscountCost(double discount){
        BigDecimal percentage = new BigDecimal("1").subtract(new BigDecimal(String.valueOf(discount)));
        double res = percentage.doubleValue() * this.getCost();
        return res;
    }

    public Boba addToppings(ArrayList<String> toppings){
        Boba boba = this;

        for(String topping : toppings){
            boba = wrapDecorator(boba, topping);
        }
        return boba;
    }

    public Boba wrapDecorator(Boba boba, String topping){
        if(topping.equals("Ice")) boba = new Ice(boba);
        else if(topping.equals("Cheese")) boba = new Cheese(boba);
        else if(topping.equals("Brown Sugar")) boba = new BrownSugar(boba);
        else if(topping.equals("Black Bubble")) boba = new BlackBubble(boba);
        return boba;
    }

    public Boba copyOfConcreteBoba(){
        String[] desc = getDescription().split(" ");
        if(desc[0].equals("Milk")){
            return new MilkBoba(UUID.randomUUID());
        }
        else if(desc[0].equals("Tea")){
            return new TeaBoba(UUID.randomUUID());
        }
        return new ClearBoba(UUID.randomUUID());
    }
}
