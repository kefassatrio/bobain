package kelompok2.kece.Bobain.service;

import kelompok2.kece.Bobain.repository.BobaRepository;

import java.util.ArrayList;

public class BobaStoreCartServiceImpl implements BobaStoreCartService{
    private final BobaRepository bobaRepository;

    public BobaStoreCartServiceImpl(BobaRepository bobaRepository) {
        this.bobaRepository = bobaRepository;
    }

    @Override
    public ArrayList<String> getAllToppings(){
        return this.bobaRepository.getAllToppings();
    }

    @Override
    public ArrayList<String> getDaftarToko(){
        return this.bobaRepository.getDaftarToko();
    }

}
