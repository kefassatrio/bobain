package kelompok2.kece.Bobain.service;

import kelompok2.kece.Bobain.model.Boba;

public interface ComboBobaService {
    Boba saveCheesyBubble();
    Boba saveIceCheeseTea();
    Boba saveIceSweetMilk();
    Boba saveIceBubbleTea();
}
