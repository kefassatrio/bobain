package kelompok2.kece.Bobain.service;

import kelompok2.kece.Bobain.model.Boba;

import java.util.ArrayList;

public interface BobaStoreCartService {
    public ArrayList<String> getDaftarToko();
    public ArrayList<String> getAllToppings();
}
