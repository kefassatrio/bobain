package kelompok2.kece.Bobain.service;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.repository.BobaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

@Service
public class BobaServiceImpl implements BobaService {
    private final BobaRepository bobaRepository;

    @Autowired
    public BobaServiceImpl(BobaRepository bobaRepository){
        this.bobaRepository = bobaRepository;
    }

    public Boba getCurrentBoba(){
        return this.bobaRepository.getCurrentBoba();
    }

    public Boba setCurrentBoba(String bobaType){
        return this.bobaRepository.setCurrentBoba(bobaType);
    }

    public void saveCurrentBoba(int jumlahBoba){
        this.bobaRepository.saveCurrentBoba(jumlahBoba);
    }

    public int getSelectedToppingsCost(){
        return this.bobaRepository.getSelectedToppingsCost();
    }

    public String getCurrentBobaType(){
        return this.bobaRepository.getCurrentBobaType();
    }

    @Override
    public Boba save(Boba savedBoba) {
        return bobaRepository.save(savedBoba);
    }

    @Override
    public Boba getBobaById(UUID id) {
        return bobaRepository.getBobaById(id);
    }

    @Override
    public ArrayList<Boba> getAllBoba() {
        return bobaRepository.getAllBoba();
    }

    public HashMap<String, String> getSelectedToppings(){
        return this.bobaRepository.getSelectedToppings();
    }

    public void addTopping(String topping){
        this.bobaRepository.addTopping(topping);
    }

    public void removeTopping(String topping){
        this.bobaRepository.removeTopping(topping);
    }

    public String getDescriptionFromSelectedToppings(){
        String res = this.bobaRepository.getCurrentBoba().getDescription();
        if(this.bobaRepository.getSelectedToppings().size() == 1){
            ArrayList<String> toppings = new ArrayList<>(this.bobaRepository.getSelectedToppings().values());
            res += " with " + toppings.get(0);
        }
        else if(this.bobaRepository.getSelectedToppings().size() > 1){
            ArrayList<String> toppings = new ArrayList<>(this.bobaRepository.getSelectedToppings().values());
            res += " with " + toppings.get(0) + ", ";

            for(int i = 1; i < toppings.size(); i++){
                res += toppings.get(i) + ", ";
            }
            res = res.substring(0, res.length() - 2);
        }
        return res;
    }

}
