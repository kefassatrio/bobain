package kelompok2.kece.Bobain.service;

import kelompok2.kece.Bobain.model.Boba;

import java.util.ArrayList;
import java.util.UUID;

public interface BobaService {
    public Boba save(Boba savedBoba);
    public Boba getBobaById(UUID id);
    public ArrayList<Boba> getAllBoba();
}
