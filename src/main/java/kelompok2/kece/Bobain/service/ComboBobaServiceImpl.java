package kelompok2.kece.Bobain.service;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.Factory.*;
import kelompok2.kece.Bobain.repository.BobaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

@Service
public class ComboBobaServiceImpl implements ComboBobaService{
    private final BobaRepository bobaRepository;
    private BobaFactory bobaFactory;

    @Autowired
    public ComboBobaServiceImpl(BobaRepository bobaRepository){
        this.bobaRepository = bobaRepository;
    }

    @Override
    public Boba saveCheesyBubble(){
        bobaFactory = new CheesyBubbleFactory();
        Boba cheesyBubble = bobaFactory.produceBoba();
        bobaRepository.saveComboBoba(cheesyBubble);
        return cheesyBubble;
    }

    @Override
    public Boba saveIceBubbleTea(){
        bobaFactory = new IceBubbleTeaFactory();
        Boba iceBubbleTea = bobaFactory.produceBoba();
        bobaRepository.saveComboBoba(iceBubbleTea);
        return iceBubbleTea;
    }

    @Override
    public Boba saveIceCheeseTea(){
        bobaFactory = new IceCheeseTeaFactory();
        Boba iceCheeseTea = bobaFactory.produceBoba();
        bobaRepository.saveComboBoba(iceCheeseTea);
        return iceCheeseTea;
    }

    @Override
    public Boba saveIceSweetMilk(){
        bobaFactory = new IceSweetMilkFactory();
        Boba iceSweetMilk = bobaFactory.produceBoba();;
        bobaRepository.saveComboBoba(iceSweetMilk);
        return iceSweetMilk;
    }
}
