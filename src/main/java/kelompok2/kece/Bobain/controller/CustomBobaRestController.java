package kelompok2.kece.Bobain.controller;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.repository.BobaRepository;
import kelompok2.kece.Bobain.service.BobaServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("rest-api")
@org.springframework.web.bind.annotation.RestController
public class CustomBobaRestController {
    private BobaRepository bobaRepository = BobainController.getBobaRepository();
    private BobaServiceImpl bobaService = new BobaServiceImpl(bobaRepository);

    @GetMapping(path = "current-boba")
    public Boba getCurrentBoba(){
        return this.bobaService.getCurrentBoba();
    }

    @GetMapping(path = "all-boba")
    public ArrayList<Boba> getAllBoba(){
        return this.bobaService.getAllBoba();
    }

    @GetMapping(path = "selected-toppings")
    public HashMap<String, String> getSelectedToppings(){
        return this.bobaService.getSelectedToppings();
    }

    @GetMapping(path = "selected-toppings-cost")
    public int getSelectedToppingsCost(){
        return this.bobaService.getSelectedToppingsCost();
    }

    @GetMapping(path = "description-from-selected-toppings")
    public String getDescriptionFromSelectedToppings(){
        return this.bobaService.getDescriptionFromSelectedToppings();
    }

    @PostMapping(path = "save")
    public void saveCurrentBoba(@RequestParam("jumlah") Integer jumlah){
        this.bobaService.saveCurrentBoba(jumlah);
    }

    @PostMapping(path = "set-boba={bobaType}")
    public void setCurrentBoba(@PathVariable("bobaType") String bobaType) {
        this.bobaService.setCurrentBoba(bobaType);
    }

    @PostMapping(path = "add-topping={topping}")
    public void addTopping(@PathVariable("topping") String topping){
        this.bobaService.addTopping(topping);
    }

    @PostMapping(path = "remove-topping={topping}")
    public void removeTopping(@PathVariable("topping") String topping){
        this.bobaService.removeTopping(topping);
    }
}
