package kelompok2.kece.Bobain.controller;

import kelompok2.kece.Bobain.receipt.controller.CartController;
import kelompok2.kece.Bobain.receipt.core.Receipt;
import kelompok2.kece.Bobain.repository.BobaRepository;
import kelompok2.kece.Bobain.service.BobaServiceImpl;
import kelompok2.kece.Bobain.service.BobaStoreCartServiceImpl;
import kelompok2.kece.Bobain.service.ComboBobaServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/")
@Controller
public class BobainController {

    private static BobaRepository bobaRepository = new BobaRepository();

    private BobaServiceImpl bobaService = new BobaServiceImpl(bobaRepository);


    private BobaStoreCartServiceImpl bobaStoreCartService = new BobaStoreCartServiceImpl(bobaRepository);

    private ComboBobaServiceImpl comboService = new ComboBobaServiceImpl(bobaRepository);

    public static BobaRepository getBobaRepository(){
        return bobaRepository;
    }


    @PostMapping(path = "api/save")
    public String saveCurrentBoba(@RequestParam("jumlah") Integer jumlah){
        this.bobaService.saveCurrentBoba(jumlah);
        return "redirect:/custom-boba";
    }

    @PostMapping(path = "api/set-boba={bobaType}")
    public String setCurrentBoba(@PathVariable("bobaType") String bobaType) {
        this.bobaService.setCurrentBoba(bobaType);
        return "redirect:/custom-boba";
    }

    @PostMapping(path = "api/add-topping={topping}")
    public String addTopping(@PathVariable("topping") String topping){
        this.bobaService.addTopping(topping);
        return "redirect:/custom-boba";
    }

    @PostMapping(path = "api/remove-topping={topping}")
    public String removeTopping(@PathVariable("topping") String topping){
        this.bobaService.removeTopping(topping);
        return "redirect:/custom-boba";
    }

    @RequestMapping(path = "/test", method = RequestMethod.GET)
    public String bobainHome(Model model) {
        model.addAttribute("currentBoba", bobaService.getCurrentBoba());
        model.addAttribute("allBoba", bobaService.getAllBoba());
        model.addAttribute("selectedToppings", bobaService.getSelectedToppings());
        return "Bobain/home";
    }

    @RequestMapping(path = "/custom-boba", method = RequestMethod.GET)
    public String customBobaPage(Model model) {
        model.addAttribute("currentBoba", bobaService.getCurrentBoba());
        model.addAttribute("currentBobaType", bobaService.getCurrentBobaType());
        model.addAttribute("allBoba", bobaService.getAllBoba());
        model.addAttribute("selectedToppings", bobaService.getSelectedToppings());
        model.addAttribute("selectedToppingsCost", bobaService.getSelectedToppingsCost());
        model.addAttribute("descriptionFromSelectedToppings", bobaService.getDescriptionFromSelectedToppings());
        return "Bobain/CustomBobaPage";
    }

    @RequestMapping(path = "/store-cart", method = RequestMethod.POST)
    public String storeCart(Model model, @RequestParam("code") String idCode) {
        model.addAttribute("daftarToko", bobaStoreCartService.getDaftarToko());
        model.addAttribute("bobaidcart", idCode.substring(0,7));
        return "Bobain/StoreCartPage";
    }

    @PostMapping(path = "api/save-ice-cheese-tea")
    public String saveIceCheeseTea(){
        this.comboService.saveIceCheeseTea();
        return "redirect:/";
    }

    @PostMapping(path = "api/save-ice-sweet-milk")
    public String saveIceSweetMilk(){
        this.comboService.saveIceSweetMilk();
        return "redirect:/";
    }

    @PostMapping(path = "api/save-ice-bubble-tea")
    public String saveIceBubbleTea(){
        this.comboService.saveIceBubbleTea();
        return "redirect:/";
    }

    @PostMapping(path = "api/save-cheesy-bubble")
    public String saveCheesyBubble(){
        this.comboService.saveCheesyBubble();
        return "redirect:/";
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String mainMenu(Model model) {
        return "Bobain/MainMenuPage";
    }
}
