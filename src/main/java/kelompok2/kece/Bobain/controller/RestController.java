package kelompok2.kece.Bobain.controller;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.receipt.controller.CartController;
import kelompok2.kece.Bobain.receipt.core.DrinkOrder;
import kelompok2.kece.Bobain.receipt.core.Order;
import kelompok2.kece.Bobain.receipt.core.Receipt;
import kelompok2.kece.Bobain.receipt.service.CartService;
import kelompok2.kece.Bobain.repository.BobaRepository;
import kelompok2.kece.Bobain.service.BobaStoreCartServiceImpl;
import kelompok2.kece.Bobain.service.ComboBobaServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("rest-api")
@org.springframework.web.bind.annotation.RestController
public class RestController {
    private BobaRepository bobaRepository = BobainController.getBobaRepository();


    private BobaStoreCartServiceImpl bobaStoreCartService = new BobaStoreCartServiceImpl(bobaRepository);

    private ComboBobaServiceImpl comboService = new ComboBobaServiceImpl(bobaRepository);

    private CartService cartService = CartController.getService();

    @GetMapping(path = "cart/get-orders")
    public List<Order> getOrders(){
        BobaRepository bobaRepo = BobainController.getBobaRepository();
        List<Boba> current = cartService.getBoba();
        for(Boba boba : bobaRepo.getAllBoba()){
            if(!current.contains(boba)) {
                cartService.addOrder(new DrinkOrder(boba));
            }
        }
        return cartService.getOrders();
    }

    @GetMapping(path = "cart/get-receipt")
    public Receipt getReceipt(){
        return cartService.getReceipt();
    }

    @GetMapping(path = "cart/get-total-price")
    public int getTotalPrice(){ return cartService.getReceipt().getTotalPrice(); }

    @GetMapping(path = "cart/get-receipts")
    public List<Receipt> getReceipts(){
        return cartService.getRepository().getReceipts();
    }

    @PostMapping(path = "save-ice-cheese-tea")
    public void saveIceCheeseTea(){
        this.comboService.saveIceCheeseTea();
    }

    @PostMapping(path = "save-ice-sweet-milk")
    public void saveIceSweetMilk(){
        this.comboService.saveIceSweetMilk();
    }

    @PostMapping(path = "save-ice-bubble-tea")
    public void saveIceBubbleTea(){
        this.comboService.saveIceBubbleTea();
    }

    @PostMapping(path = "save-cheesy-bubble")
    public void saveCheesyBubble(){ this.comboService.saveCheesyBubble(); }

    @PostMapping(path = "cart/delete")
    public void deleteOrder(@RequestParam("index") Integer index){
        cartService.removeOrder(index);
        bobaRepository.clearRepo();
        for(Boba boba : cartService.getBoba()){
            bobaRepository.save(boba);
        }
    }

    @PostMapping(path = "cart/discount")
    public void discount(@RequestParam("code") String code){
        cartService.verifyDiscount(code);
    }

    @PostMapping(path = "cart/clear-order")
    public void clearOrder() {
        bobaRepository.clearRepo();
        cartService.clearOrder();
    }

    @PostMapping(path = "cart/store-receipt")
    public void storeReceipt() {
        bobaRepository.clearRepo();
        cartService.storeReceipt();
    }

    @GetMapping(path = "store-cart-page")
    public ArrayList<String> storeCart(){ return this.bobaStoreCartService.getDaftarToko(); }

    @GetMapping(path = "boba-id-cart")
    public String bobaIdCart(){
        return CartController.getService().getReceipt().getId().toString().substring(0,7);
    }
}
