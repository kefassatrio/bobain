package kelompok2.kece.Bobain.receipt.controller;

import kelompok2.kece.Bobain.controller.BobainController;
import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.receipt.core.DrinkOrder;
import kelompok2.kece.Bobain.receipt.core.Order;
import kelompok2.kece.Bobain.receipt.core.Receipt;
import kelompok2.kece.Bobain.receipt.repository.ReceiptRepository;
import kelompok2.kece.Bobain.receipt.service.CartService;
import kelompok2.kece.Bobain.receipt.service.CartServiceImpl;

import kelompok2.kece.Bobain.repository.BobaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/cart")
public class CartController {
    private static ReceiptRepository repository = new ReceiptRepository();
    private static CartService service = new CartServiceImpl(repository);

    public static CartService getService(){
        return service;
    }

    @GetMapping(path = "")
    public String cartView(Model model){
        BobaRepository bobaRepo = BobainController.getBobaRepository();
        List<Boba> current = service.getBoba();
        for(Boba boba : bobaRepo.getAllBoba()){
            if(!current.contains(boba)) {
                service.addOrder(new DrinkOrder(boba));
            }
        }
        model.addAttribute("receipt", service.getReceipt());
        model.addAttribute("orders", service.getOrders());
        return "Bobain/CartPage";
    }

    @GetMapping(path = "/history")
    public String historyView(Model model){
        model.addAttribute("receipts", repository.getReceipts());
        return "Bobain/HistoryPage";
    }

    @PostMapping(path = "/delete")
    public String deleteOrder(@RequestParam("index") String index){
        BobaRepository bobaRepo = BobainController.getBobaRepository();
        service.removeOrder(Integer.parseInt(index));
        bobaRepo.clearRepo();
        for(Boba boba : service.getBoba()){
            bobaRepo.save(boba);
        }
        return "redirect:/cart";
    }

    @PostMapping(path = "/discount")
    public String discount(@RequestParam("discount") String code){
        service.verifyDiscount(code);
        return "redirect:/cart";
    }

    @PostMapping(path = "/store-receipt")
    public String storeReceipt(){
        BobaRepository bobaRepo = BobainController.getBobaRepository();
        bobaRepo.clearRepo();
        service.storeReceipt();
        return "redirect:/";
    }

    @PostMapping(path = "/clear-order")
    public String clearOrder(){
        BobaRepository bobaRepo = BobainController.getBobaRepository();
        bobaRepo.clearRepo();
        service.clearOrder();
        return "redirect:/cart";
    }
}
