package kelompok2.kece.Bobain.receipt.core;

import java.util.List;
import java.util.UUID;

public interface Receipt {
    List<Order> getOrders();
    void addOrder(Order order);
    void removeOrder(Order order);
    void clearOrder();
    int getQuantity();
    void discount(Double discount);
    int getTotalPrice();
    List<String> getDescriptions();
    List<Integer> getPrices();
    UUID getId();
    void setId(UUID id);
}
