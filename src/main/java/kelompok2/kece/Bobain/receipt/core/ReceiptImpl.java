package kelompok2.kece.Bobain.receipt.core;

import java.util.List;
import java.util.ArrayList;
import java.util.UUID;

public class ReceiptImpl implements Receipt {
    private UUID id;
    private List<Order> orders;

    public ReceiptImpl(){
        this.id = UUID.randomUUID();
        this.orders = new ArrayList<Order>();
    }

    @Override
    public List<Order> getOrders() {
        return orders;
    }

    @Override
    public void addOrder(Order order) {
        orders.add(order);
    }

    @Override
    public void removeOrder(Order order) {
        orders.remove(order);
    }

    @Override
    public void clearOrder() {
        orders.clear();
    }

    @Override
    public int getQuantity(){
        return orders.size();
    }

    @Override
    public void discount(Double discount) {
        for(Order order : orders){
            order.discount(discount);
        }
    }

    @Override
    public int getTotalPrice() {
        int total = 0;
        for(Order order : orders){
            total += order.getPrice();
        }
        return total;
    }

    @Override
    public UUID getId(){
        return this.id;
    }

    @Override
    public void setId(UUID id){
        this.id = id;
    }

    @Override
    public List<String> getDescriptions(){
        List<String> result = new ArrayList();
        for(Order order : orders){
            result.add(order.getDescription());
        }
        return result;
    }

    @Override
    public List<Integer> getPrices(){
        List<Integer> result = new ArrayList();
        for(Order order : orders){
            result.add(order.getPrice());
        }
        return result;
    }
}
