package kelompok2.kece.Bobain.receipt.core;

import kelompok2.kece.Bobain.model.*;

public class DrinkOrder implements Order {
    Boba drink;
    String desc;
    int price;

    public DrinkOrder(Boba drink){
        this.drink = drink;
        this.desc = drink.getDescription();
        this.price = drink.getCost();
    }

    public void setDrink(Boba boba){
        this.drink = boba;
    }

    public Boba getDrink(){
        return this.drink;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void discount(Double discount) {
        int discountVal = (int)drink.getDiscountCost(discount);
        this.price = drink.getCost() - discountVal;
    }

    @Override
    public String getDescription(){
        return this.desc;
    }
}
