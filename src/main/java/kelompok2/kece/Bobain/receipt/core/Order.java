package kelompok2.kece.Bobain.receipt.core;

import kelompok2.kece.Bobain.model.*;

public interface Order {
    void setPrice(int price);
    int getPrice();
    void discount(Double discount);
    String getDescription();
}
