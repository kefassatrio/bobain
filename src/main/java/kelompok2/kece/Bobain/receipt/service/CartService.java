package kelompok2.kece.Bobain.receipt.service;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.receipt.core.Order;
import kelompok2.kece.Bobain.receipt.core.Receipt;
import kelompok2.kece.Bobain.receipt.repository.ReceiptRepository;

import java.util.List;

public interface CartService {
    ReceiptRepository getRepository();
    Receipt getReceipt();
    void storeReceipt();
    List<Order> getOrders();
    List<Boba> getBoba();
    void clearOrder();
    void addOrder(Order order);
    Order getOrder(int index);
    void removeOrder(Order order);
    void removeOrder(int index);
    void addDiscountCode(String code, double discount);
    double getDiscountCode(String code);
    void discount(double discount);
    void verifyDiscount(String code);
}
