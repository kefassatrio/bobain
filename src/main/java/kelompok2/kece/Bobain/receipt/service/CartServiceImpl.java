package kelompok2.kece.Bobain.receipt.service;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.ConcreteBoba.MilkBoba;
import kelompok2.kece.Bobain.model.ConcreteBoba.TeaBoba;
import kelompok2.kece.Bobain.model.Decorator.BlackBubble;
import kelompok2.kece.Bobain.model.Decorator.BrownSugar;
import kelompok2.kece.Bobain.model.Decorator.Cheese;
import kelompok2.kece.Bobain.model.Decorator.Ice;
import kelompok2.kece.Bobain.receipt.core.DrinkOrder;
import kelompok2.kece.Bobain.receipt.core.Order;
import kelompok2.kece.Bobain.receipt.core.Receipt;
import kelompok2.kece.Bobain.receipt.core.ReceiptImpl;
import kelompok2.kece.Bobain.receipt.repository.ReceiptRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class CartServiceImpl implements CartService {
    private ReceiptRepository receiptRepository;
    private Receipt receipt;
    private Map<String, Double> discountCodeMap;

    @Autowired
    public CartServiceImpl(ReceiptRepository receiptRepository){
        this.receiptRepository = receiptRepository;
        this.receipt = new ReceiptImpl();
        this.discountCodeMap = new HashMap<String, Double>();
        seed();
    }

    private void seed(){
        /*
        Order order1 = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Order order2 = new DrinkOrder(new MilkBoba(UUID.randomUUID()));
        Order order3 = new DrinkOrder(new TeaBoba(UUID.randomUUID()));

        receipt.addOrder(order1);
        receipt.addOrder(order2);
        receipt.addOrder(order3);

        Receipt receipt1 = new ReceiptImpl();
        Receipt receipt2 = new ReceiptImpl();

        Order order4 = new DrinkOrder(new BlackBubble(new ClearBoba(UUID.randomUUID())));
        Order order5 = new DrinkOrder(new BrownSugar(new MilkBoba(UUID.randomUUID())));
        Order order6 = new DrinkOrder(new Cheese(new TeaBoba(UUID.randomUUID())));
        Order order7 = new DrinkOrder(new Ice(new MilkBoba(UUID.randomUUID())));

        receipt1.addOrder(order4);
        receipt1.addOrder(order5);
        receipt2.addOrder(order6);
        receipt2.addOrder(order7);

        receiptRepository.addReceipt(receipt1);
        receiptRepository.addReceipt(receipt2);
        */

        discountCodeMap.put("PROMO80", 0.8);
        discountCodeMap.put("PROMO60", 0.6);
        discountCodeMap.put("SETENGAHSETENGAH", 0.5);
    }

    @Override
    public ReceiptRepository getRepository(){
        return this.receiptRepository;
    }

    @Override
    public Receipt getReceipt() {
        return receipt;
    }

    @Override
    public void storeReceipt() {
        this.receiptRepository.addReceipt(receipt);
        receipt = new ReceiptImpl();
    }

    @Override
    public List<Order> getOrders(){
        return receipt.getOrders();
    }

    @Override
    public List<Boba> getBoba(){
        List<Boba> result = new ArrayList<>();
        for(Order order : receipt.getOrders()){
            try{
                result.add(((DrinkOrder)order).getDrink());
            }catch (Exception ignored){

            }
        }
        return result;
    }

    @Override
    public void clearOrder() {
        receipt.clearOrder();
    }

    @Override
    public void addOrder(Order order){
        receipt.addOrder(order);
    }

    @Override
    public Order getOrder(int index){
        return this.getOrders().get(index);
    }

    @Override
    public void removeOrder(Order order){
        receipt.removeOrder(order);
    }

    @Override
    public void removeOrder(int index){
        try{
            Order order = receipt.getOrders().get(index);
            removeOrder(order);
        } catch (IndexOutOfBoundsException ignored){

        }
    }

    @Override
    public void addDiscountCode(String code, double discount){
        this.discountCodeMap.put(code, discount);
    }

    @Override
    public double getDiscountCode(String code){
        return this.discountCodeMap.get(code);
    }

    @Override
    public void discount(double discount) {
        receipt.discount(discount);
    }

    @Override
    public void verifyDiscount(String code){
        if(this.discountCodeMap.containsKey(code)){
            discount(this.discountCodeMap.get(code));
        }
    }
}
