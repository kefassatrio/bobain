package kelompok2.kece.Bobain.receipt.repository;

import org.springframework.stereotype.Repository;

import kelompok2.kece.Bobain.receipt.core.Receipt;

import java.util.*;

@Repository
public class ReceiptRepository {
    private Map<UUID, Receipt> receiptMap = new HashMap();

    public void addReceipt(Receipt receipt){
        receiptMap.put(receipt.getId(), receipt);
    }

    public void deleteReceipt(UUID id){
        receiptMap.remove(id);
    }

    public Map<UUID, Receipt> getReceiptMap(){
        return receiptMap;
    }

    public List<Receipt> getReceipts(){
        List<Receipt> receipts = new ArrayList();
        for(Receipt receipt : receiptMap.values()){
            receipts.add(receipt);
        }
        return receipts;
    }

    public Map<String, UUID> getIdMap(){
        Map<String, UUID> idMap = new HashMap();
        for(UUID id : receiptMap.keySet()){
            idMap.put(id.toString(), id);
        }
        return idMap;
    }
}
