function toggleLoading(){
    $("#loading-screen").toggleClass("not-shown");
}

function loadCart(){
    $.get("/rest-api/cart/get-orders",
        function(result){
            console.log("fetch orders");
            console.log(result);
            if($.trim($("#order-body").html()).length != 0){
                $("#order-body").empty();
            }
            for(i = 0; i < result.length; i++){
                var desc = result[i].description;
                var price = result[i].price;
                $("#order-body").append("<tr><td>"+desc+"</td><td>"+price+"</td><td><button class=\"btn btn-outline-dark\" onclick=\"deleteOrder("+i+")\">Delete</button></td></tr>");
            }
        }
    );
    $.get("/rest-api/cart/get-total-price",
        function(result){
            console.log("fetch total price");
            console.log(result);
            $("#total-price").empty().text("Total : " + result);
        }
    );
    console.log("success load");
}

function deleteOrder(indexVal){
    console.log("delete order " + indexVal);
    $.post("/rest-api/cart/delete",
        {
            index : indexVal
        },
        function(){
            console.log("success delete");
            loadCart();
        }
    );
}

function discountFunction(){
    var codeVal = $("#discount").val();
    console.log("discount " + discount);
    $.post("/rest-api/cart/discount",
        {
            code : codeVal
        },
        function(){
            console.log("post discount success");
            loadCart();
        }
    );
}

function clearOrder(){
    console.log("clear order");
    $.post("/rest-api/cart/clear-order",
        function(){
            console.log("success clear order");
            loadCart();
        }
    );
}

function storeReceipt(){
    console.log("store receipt");
    $.post("/rest-api/cart/store-receipt",
        function(){
            console.log("success store receipt");
            loadCart();
        }
    );
}

$(document).ready(function(){
    loadCart();
});

$(document).on("click", "#discount-button", function(event){
    event.preventDefault();
    discountFunction();
});