function alertSaved(){
    alert("Your boba is saved to cart :)")
}

function saveCheesyBubble(){
    $('#loading-screen').toggleClass("not-shown");

    console.log("Cheesy Bubble");
    $.post("/rest-api/save-cheesy-bubble",
        function(){
            setTimeout(() => {
                $('#loading-screen').toggleClass("not-shown");
                alertSaved();
            }, 2000);
            console.log("Cheesy Bubble Saved")
        });
}

function saveIceSweetMilk(){
    $('#loading-screen').toggleClass("not-shown");

    console.log("Ice Sweet Milk");
    $.post("/rest-api/save-ice-sweet-milk",
        function(){
            setTimeout(() => {
                $('#loading-screen').toggleClass("not-shown");
                alertSaved();
            }, 2000);
            console.log("Ice Sweet Milk Saved")
        });
}

function saveIceBubbleTea(){
    $('#loading-screen').toggleClass("not-shown");

    console.log("Ice Bubble Tea");
    $.post("/rest-api/save-ice-bubble-tea",
        function(){
            setTimeout(() => {
                $('#loading-screen').toggleClass("not-shown");
                alertSaved();
            }, 2000);
            console.log("Ice Bubble Tea Saved")
        });
}

function saveIceCheeseTea(){
    $('#loading-screen').toggleClass("not-shown");

    console.log("Ice Cheese Tea");
    $.post("/rest-api/save-ice-cheese-tea",
        function(){
            setTimeout(() => {
                $('#loading-screen').toggleClass("not-shown");
                alertSaved();
            }, 2000);
            console.log("Ice Cheese Tea Saved")
        });
}

$(document).ready(function(){

});