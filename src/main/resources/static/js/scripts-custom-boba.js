function alertSaved(){
    alert("Your boba is saved to cart :)")
}

function saveBoba(){
    $('#loading-screen').toggleClass("not-shown");

    var selectedJumlah = $("#save-to-cart-input").val();

    $.post("/rest-api/save",
      {
        jumlah: selectedJumlah
      },
      function(){
        $.get(
            "/rest-api/current-boba",
            function(res){
                $('#selected-toppings').empty();
                $('#selected-base-img').attr("src", `/img/${res.type}.png`);
                $('#selected-base-img').attr("alt", res.description);
                $('#selected-base-desc-title').text(res.description);
                $('#selected-base-desc-cost').text(res.cost);
                setTimeout(() => {
                            $('#loading-screen').toggleClass("not-shown");
                            alertSaved();
                        }, 2000);
            }
        );
      });
}

$(document).ready(function(){
  
});