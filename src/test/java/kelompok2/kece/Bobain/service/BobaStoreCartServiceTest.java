package kelompok2.kece.Bobain.service;

import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.Decorator.BlackBubble;
import kelompok2.kece.Bobain.model.Decorator.BrownSugar;
import kelompok2.kece.Bobain.model.Decorator.Cheese;
import kelompok2.kece.Bobain.repository.BobaRepository;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

public class BobaStoreCartServiceTest {
    private BobaRepository bobaRepository;

    private BobaStoreCartServiceImpl bobaStoreCartService;
    private BobaService bobaService;

    @Before
    public void setup(){
        bobaRepository = new BobaRepository();
        bobaStoreCartService = new BobaStoreCartServiceImpl(bobaRepository);
        bobaService = new BobaServiceImpl(bobaRepository);
    }

    @Test
    public void testGetAllToppings(){
        bobaService.save(new Cheese(new BrownSugar(new BlackBubble(new ClearBoba(UUID.randomUUID())))));
        assertEquals(bobaStoreCartService.getAllToppings().get(2), "Cheese");
        assertEquals(bobaStoreCartService.getAllToppings().get(1), "Brown Sugar");
        assertEquals(bobaStoreCartService.getAllToppings().get(0), "Black Bubble");
    }

    @Test
    public void testGetDaftarToko(){
        bobaService.save(new Cheese(new BrownSugar(new BlackBubble(new ClearBoba(UUID.randomUUID())))));
        assertTrue(bobaStoreCartService.getDaftarToko().contains("BobaIn Boba Store"));
        assertThat(bobaStoreCartService.getDaftarToko()).isNotNull();
    }

}
