package kelompok2.kece.Bobain.service;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.repository.BobaRepository;
import org.junit.Before;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ComboBobaServiceTest {
    private BobaRepository bobaRepository;

    private ComboBobaServiceImpl comboService;

    @Before
    public void setUp(){
        bobaRepository = new BobaRepository();
        comboService = new ComboBobaServiceImpl(bobaRepository);
    }

    @Test
    public void testSaveCheesyBubble(){
        Boba boba = comboService.saveCheesyBubble();
        assertTrue(boba instanceof Boba);
        System.out.println(boba.getDescription());
        assertTrue(boba.getDescription().equals("Clear Boba with Black Bubble, Cheese"));
    }

    @Test
    public void testSaveIceCheeseTea(){
        Boba boba = comboService.saveIceCheeseTea();
        assertTrue(boba instanceof Boba);
        assertTrue(boba.getDescription().equals("Tea Boba with Cheese, Ice"));
    }

    @Test
    public void testSaveIceSweetMilk(){
        Boba boba = comboService.saveIceSweetMilk();
        assertTrue(boba instanceof Boba);
        assertTrue(boba.getDescription().equals("Milk Boba with Brown Sugar, Ice"));
    }

    @Test
    public void testSaveIceBubbleTea(){
        Boba boba = comboService.saveIceBubbleTea();
        assertTrue(boba instanceof Boba);
        assertTrue(boba.getDescription().equals("Tea Boba with Black Bubble, Ice"));
    }
}
