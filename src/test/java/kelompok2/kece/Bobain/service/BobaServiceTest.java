package kelompok2.kece.Bobain.service;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.repository.BobaRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BobaServiceTest {
    private BobaRepository bobaRepository;

    private BobaServiceImpl bobaService;

    @Before
    public void setUp(){
        bobaRepository = new BobaRepository();
        bobaService = new BobaServiceImpl(bobaRepository);
    }

    @Test
    public void testGetAllBoba(){
        Boba boba = new ClearBoba(UUID.randomUUID());
        bobaService.save(boba);
        assertEquals(bobaService.getAllBoba().size(), 1);
    }

    @Test
    public void testSaveBoba(){
        Boba boba = new ClearBoba(UUID.randomUUID());
        Boba savedBoba = bobaService.save(boba);
        assertEquals(boba, savedBoba);
    }

    @Test
    public void testGetBobaById(){
        UUID id = UUID.randomUUID();
        Boba boba = new ClearBoba(id);
        bobaService.save(boba);
        assertEquals(bobaService.getBobaById(id), boba);
    }

    @Test
    public void testGetSelectedToppings(){
        assertThat(bobaService.getSelectedToppings()).isNotNull();
    }

    @Test
    public void testAddToppingIce(){
        bobaService.addTopping("ice");
        assertEquals(bobaService.getSelectedToppings().get("ice"), "Ice");
    }

    @Test
    public void testAddToppingCheese(){
        bobaService.addTopping("cheese");
        assertEquals(bobaService.getSelectedToppings().get("cheese"), "Cheese");
    }

    @Test
    public void testAddToppingBrownSugar(){
        bobaService.addTopping("brown-sugar");
        assertEquals(bobaService.getSelectedToppings().get("brown-sugar"), "Brown Sugar");
    }

    @Test
    public void testAddToppingBlackBubble(){
        bobaService.addTopping("black-bubble");
        assertEquals(bobaService.getSelectedToppings().get("black-bubble"), "Black Bubble");
    }

    @Test
    public void testRemoveToppingIce(){
        bobaService.removeTopping("ice");
        assertEquals(bobaService.getSelectedToppings().containsKey("ice"), false);
    }

    @Test
    public void testRemoveToppingCheese(){
        bobaService.removeTopping("cheese");
        assertEquals(bobaService.getSelectedToppings().containsKey("cheese"), false);
    }

    @Test
    public void testRemoveToppingBrownSugar(){
        bobaService.removeTopping("brown-sugar");
        assertEquals(bobaService.getSelectedToppings().containsKey("brown-sugar"), false);
    }

    @Test
    public void testRemoveToppingBlackBubble(){
        bobaService.removeTopping("black-bubble");
        assertEquals(bobaService.getSelectedToppings().containsKey("black-bubble"), false);
    }


    @Test
    public void testGetCurrentBobaDefaultIsClearBoba(){
        assertEquals(bobaService.getCurrentBoba().getDescription(), "Clear Boba");
    }

    @Test
    public void testGetSelectedToppingsCost(){
        assertEquals(bobaService.getSelectedToppingsCost(), 0);
    }

    @Test
    public void testGetCurrentBobaType(){
        assertEquals(bobaRepository.getCurrentBobaType(), "Clear");
    }

    @Test
    public void testSetCurrentBobaToClearBoba(){
        assertEquals(bobaService.setCurrentBoba("clear").getDescription(), "Clear Boba");
        assertEquals(bobaService.getCurrentBoba().getDescription(), "Clear Boba");
    }

    @Test
    public void testSetCurrentBobaToMilkBoba(){
        assertEquals(bobaService.setCurrentBoba("milk").getDescription(), "Milk Boba");
        assertEquals(bobaService.getCurrentBoba().getDescription(), "Milk Boba");
    }

    @Test
    public void testSetCurrentBobaToTeaBoba(){
        assertEquals(bobaService.setCurrentBoba("tea").getDescription(), "Tea Boba");
        assertEquals(bobaService.getCurrentBoba().getDescription(), "Tea Boba");
    }


    @Test
    public void testSaveCurrentBoba(){
        bobaService.setCurrentBoba("clear");
        bobaService.addTopping("black-bubble");
        bobaService.addTopping("brown-sugar");
        bobaService.saveCurrentBoba(2);
        assertEquals(bobaService.getAllBoba().size(), 2);
        assertEquals(bobaRepository.getAllBoba().get(0).getType(), "Clear");
        assertEquals(bobaRepository.getAllBoba().get(0).getToppings().contains("Black Bubble"), true);
        assertEquals(bobaRepository.getAllBoba().get(0).getToppings().contains("Brown Sugar"), true);
        assertEquals(bobaRepository.getAllBoba().get(1).getType(), "Clear");
        assertEquals(bobaRepository.getAllBoba().get(1).getToppings().contains("Black Bubble"), true);
        assertEquals(bobaRepository.getAllBoba().get(1).getToppings().contains("Brown Sugar"), true);
    }

    @Test
    public void testGetDescriptionFromSelectedToppings1Topping(){
        bobaService.setCurrentBoba("clear");
        bobaService.addTopping("black-bubble");
        assertEquals(bobaService.getDescriptionFromSelectedToppings(), "Clear Boba with Black Bubble");
    }

    @Test
    public void testGetDescriptionFromSelectedToppings2Topping(){
        bobaService.setCurrentBoba("clear");
        bobaService.addTopping("black-bubble");
        bobaService.addTopping("brown-sugar");
        assertEquals(bobaService.getDescriptionFromSelectedToppings(), "Clear Boba with Brown Sugar, Black Bubble");
    }
}
