package kelompok2.kece.Bobain.model.ConcreteBoba;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MilkBobaTest {
    private MilkBoba boba;

    @BeforeEach
    public void setUp(){
    }


    @Test
    public void testGetDescription() throws Exception {
        this.boba = new MilkBoba(UUID.randomUUID());
        assertEquals(this.boba.getDescription(), "Milk Boba");
    }

    @Test
    public void testGetType(){
        this.boba = new MilkBoba(UUID.randomUUID());
        assertEquals(this.boba.getType(), "Milk");
    }

    @Test
    public void testGetCost() throws Exception {
        this.boba = new MilkBoba(UUID.randomUUID());
        assertEquals(this.boba.getCost(), 17000);
    }
}
