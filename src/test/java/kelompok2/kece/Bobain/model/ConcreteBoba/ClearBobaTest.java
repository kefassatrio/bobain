package kelompok2.kece.Bobain.model.ConcreteBoba;

import kelompok2.kece.Bobain.model.Boba;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;
import java.util.UUID;

public class ClearBobaTest {
    private Boba boba;

    @Before
    public void setUp(){
        this.boba = new ClearBoba(UUID.randomUUID());
    }


    @Test
    public void testGetDescription() throws Exception {
        assertEquals(this.boba.getDescription(), "Clear Boba");
    }

    @Test
    public void testGetCost() throws Exception {
        this.boba = new ClearBoba(UUID.randomUUID());
        assertEquals(this.boba.getCost(), 10000);
    }

    @Test
    public void testGetId() throws Exception {
        UUID id = UUID.randomUUID();
        this.boba = new ClearBoba(id);
        assertEquals(this.boba.getId(), id);
    }

    @Test
    public void testGetDiscount() throws Exception {
        UUID id = UUID.randomUUID();
        this.boba = new ClearBoba(id);
        assertEquals(this.boba.getDiscountCost(0.1), 0.9*this.boba.getCost());
    }

    @Test
    public void testAddToppings() throws Exception{
        UUID id = UUID.randomUUID();
        this.boba = new ClearBoba(id);
        ArrayList<String> toppings = new ArrayList<>();
        toppings.add("Ice");
        toppings.add("Black Bubble");
        toppings.add("Cheese");

        this.boba = this.boba.addToppings(toppings);
        assertEquals(this.boba.getDescription(), "Clear Boba with Ice, Black Bubble, Cheese");
    }

    @Test
    public void testCopyOfConcreteBoba() throws Exception{
        this.boba = new ClearBoba(UUID.randomUUID());
        Boba anotherBoba = this.boba.copyOfConcreteBoba();

        assertEquals(this.boba.getDescription(), anotherBoba.getDescription());
        assertNotEquals(this.boba.getId(), anotherBoba.getId());
        assertNotEquals(this.boba, anotherBoba);
    }

    @Test
    public void testWrapDecoratorIce() throws Exception{
        this.boba = new ClearBoba(UUID.randomUUID());
        assertEquals(this.boba.wrapDecorator(this.boba, "Ice").getDescription(), "Clear Boba with Ice");
    }

    @Test
    public void testWrapDecoratorCheese() throws Exception{
        this.boba = new ClearBoba(UUID.randomUUID());
        assertEquals(this.boba.wrapDecorator(this.boba, "Cheese").getDescription(), "Clear Boba with Cheese");
    }

    @Test
    public void testWrapDecoratorBrownSugar() throws Exception{
        this.boba = new ClearBoba(UUID.randomUUID());
        assertEquals(this.boba.wrapDecorator(this.boba, "Brown Sugar").getDescription(), "Clear Boba with Brown Sugar");
    }

    @Test
    public void testGetType(){
        this.boba = new ClearBoba(UUID.randomUUID());
        assertEquals(this.boba.getType(), "Clear");
    }

    @Test
    public void testWrapDecoratorBlackBubble() throws Exception{
        this.boba = new ClearBoba(UUID.randomUUID());
        assertEquals(this.boba.wrapDecorator(this.boba, "Black Bubble").getDescription(), "Clear Boba with Black Bubble");
    }
}
