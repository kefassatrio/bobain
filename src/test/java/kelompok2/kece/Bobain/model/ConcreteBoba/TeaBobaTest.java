package kelompok2.kece.Bobain.model.ConcreteBoba;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

public class TeaBobaTest {
    private TeaBoba boba;

    @BeforeEach
    public void setUp(){
    }


    @Test
    public void testGetDescription() throws Exception {
        this.boba = new TeaBoba(UUID.randomUUID());
        assertEquals(this.boba.getDescription(), "Tea Boba");
    }

    @Test
    public void testGetCost() throws Exception {
        this.boba = new TeaBoba(UUID.randomUUID());
        assertEquals(this.boba.getCost(), 15000);
    }

    @Test
    public void testGetType(){
        this.boba = new TeaBoba(UUID.randomUUID());
        assertEquals(this.boba.getType(), "Tea");
    }
}
