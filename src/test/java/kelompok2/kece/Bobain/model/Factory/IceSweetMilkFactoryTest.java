package kelompok2.kece.Bobain.model.Factory;

import kelompok2.kece.Bobain.model.Boba;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IceSweetMilkFactoryTest{

    private Boba boba;
    private IceSweetMilkFactory iceSweetMilkFactory = new IceSweetMilkFactory();

    @Test
    public void testProduceCheeseTea() throws Exception {
        assertTrue(iceSweetMilkFactory.produceBoba() instanceof Boba);
    }

    @Test
    public void testGetCost() throws Exception {
        boba = iceSweetMilkFactory.produceBoba();
        assertEquals(boba.getCost(), 25000);
    }

    @Test
    public void testGetDescription() throws Exception {
        boba = iceSweetMilkFactory.produceBoba();
        assertEquals(boba.getDescription(), "Milk Boba with Brown Sugar, Ice");
    }

    @Test
    public void testGetTitle() throws Exception {
        boba = iceSweetMilkFactory.produceBoba();
        assertEquals(iceSweetMilkFactory.getTitle(), "Ice Sweet Milk");
    }
}