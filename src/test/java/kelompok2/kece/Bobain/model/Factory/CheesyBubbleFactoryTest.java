package kelompok2.kece.Bobain.model.Factory;

import kelompok2.kece.Bobain.model.Boba;
import org.junit.Test;
import org.springframework.test.context.TestExecutionListeners;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class CheesyBubbleFactoryTest {

    private Boba boba;
    private CheesyBubbleFactory cheesyBubbleFactory = new CheesyBubbleFactory();

    @Test
    public void testProduceCheeseTea() throws Exception {
        assertTrue(cheesyBubbleFactory.produceBoba() instanceof Boba);
    }

    @Test
    public void testGetCost() throws Exception {
        boba = cheesyBubbleFactory.produceBoba();
        assertEquals(boba.getCost(), 20000);
    }

    @Test
    public void testGetDescription() throws Exception {
        boba = cheesyBubbleFactory.produceBoba();
        assertEquals(boba.getDescription(), "Clear Boba with Black Bubble, Cheese");
    }

    @Test
    public void testGetTitle() throws Exception {
        assertEquals(cheesyBubbleFactory.getTitle(), "Cheesy Bubble");
    }
}