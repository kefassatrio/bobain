package kelompok2.kece.Bobain.model.Factory;

import kelompok2.kece.Bobain.model.Boba;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IceCheeseTeaFactoryTest{

    private Boba boba;
    private IceCheeseTeaFactory iceCheeseTeaFactory = new IceCheeseTeaFactory();

    @Test
    public void testProduceCheeseTea() throws Exception {
        assertTrue(iceCheeseTeaFactory.produceBoba() instanceof Boba);
    }

    @Test
    public void testGetCost() throws Exception {
        boba = iceCheeseTeaFactory.produceBoba();
        assertEquals(boba.getCost(), 23000);
    }

    @Test
    public void testGetDescription() throws Exception {
        boba = iceCheeseTeaFactory.produceBoba();
        assertEquals(boba.getDescription(), "Tea Boba with Cheese, Ice");
    }

    @Test
    public void testGetTitle() throws Exception {
        boba = iceCheeseTeaFactory.produceBoba();
        assertEquals(iceCheeseTeaFactory.getTitle(), "Ice Cheese Tea");
    }
}
