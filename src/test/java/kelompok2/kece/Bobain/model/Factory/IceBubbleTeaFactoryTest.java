package kelompok2.kece.Bobain.model.Factory;

import kelompok2.kece.Bobain.model.Boba;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IceBubbleTeaFactoryTest {

    private Boba boba;
    private IceBubbleTeaFactory iceBubbleTeaFactory = new IceBubbleTeaFactory();

    @Test
    public void testProduceCheeseTea() throws Exception {
        assertTrue(iceBubbleTeaFactory.produceBoba() instanceof Boba);
    }

    @Test
    public void testGetCost() throws Exception {
        boba = iceBubbleTeaFactory.produceBoba();
        assertEquals(boba.getCost(), 23000);
    }

    @Test
    public void testGetDescription() throws Exception {
        boba = iceBubbleTeaFactory.produceBoba();
        assertEquals(boba.getDescription(), "Tea Boba with Black Bubble, Ice");
    }

    @Test
    public void testGetTitle() throws Exception {
        assertEquals(iceBubbleTeaFactory.getTitle(), "Ice Bubble Tea");
    }
}
