package kelompok2.kece.Bobain.model.CoR;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class KoiHandlerTest {

    private ArrayList<String> testToppings = new ArrayList<>();
    private Handler nextHandler;

    @BeforeEach
    public void setup(){
    }

    @Test
    public void testSetNextHandler() throws Exception{
        Handler testHandler = new KoiHandler();
        this.nextHandler = new XingfutangHandler();
        testHandler.setNextHandler(this.nextHandler);
        assertEquals(testHandler.getNextHandler(), this.nextHandler);
    }

    @Test
    public void testHandle() throws Exception{
        HandlerAttribute testHandler = new BobainHandler();
        Handler chatimeHandler = new ChatimeHandler();
        Handler hophopHandler = new HophopHandler();
        Handler koiHandler = new KoiHandler();
        Handler xingfutangHandler = new XingfutangHandler();
        testHandler.setNextHandler(chatimeHandler);
        chatimeHandler.setNextHandler(hophopHandler);
        hophopHandler.setNextHandler(koiHandler);
        koiHandler.setNextHandler(xingfutangHandler);
        testToppings.add("Black Bubble");
        testToppings.add("Brown Sugar");
        testToppings.add("Cheese");
        testHandler.handle(testToppings, testHandler.daftarToko);
        assertTrue(testHandler.daftarToko.contains("Koi Boba Store"));
    }
}