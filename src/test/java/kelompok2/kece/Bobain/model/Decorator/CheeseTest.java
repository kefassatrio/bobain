package kelompok2.kece.Bobain.model.Decorator;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheeseTest {
    private Boba boba;

    @BeforeEach
    public void setUp(){
    }


    @Test
    public void testGetDescription() throws Exception {
        this.boba = new ClearBoba(UUID.randomUUID());
        this.boba = new Cheese(this.boba);
        assertEquals(this.boba.getDescription(), "Clear Boba with Cheese");
    }

    @Test
    public void testMultipleGetDescription() throws Exception {
        this.boba = new ClearBoba(UUID.randomUUID());
        this.boba = new Cheese(this.boba);
        this.boba = new BrownSugar(this.boba);
        this.boba = new Ice(this.boba);

        assertEquals(this.boba.getDescription(), "Clear Boba with Cheese, Brown Sugar, Ice");
    }

    @Test
    public void testGetCost() throws Exception {
        this.boba = new ClearBoba(UUID.randomUUID());
        this.boba = new Cheese(this.boba);
        assertEquals(this.boba.getCost(), 15000);
    }

    @Test
    public void testMultipleGetCost() throws Exception {
        this.boba = new ClearBoba(UUID.randomUUID());
        this.boba = new BlackBubble(this.boba);
        this.boba = new BrownSugar(this.boba);
        this.boba = new Cheese(this.boba);
        assertEquals(this.boba.getCost(), 25000);
    }
}