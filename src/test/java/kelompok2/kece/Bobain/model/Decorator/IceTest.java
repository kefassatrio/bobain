package kelompok2.kece.Bobain.model.Decorator;

import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.Boba;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

public class IceTest {
    private Boba boba;

    @BeforeEach
    public void setUp(){
    }


    @Test
    public void testGetDescription() throws Exception {
        this.boba = new ClearBoba(UUID.randomUUID());
        this.boba = new Ice(this.boba);
        assertEquals(this.boba.getDescription(), "Clear Boba with Ice");
    }

    @Test
    public void testGetType() throws Exception {
        this.boba = new ClearBoba(UUID.randomUUID());
        this.boba = new Ice(this.boba);
        assertEquals(this.boba.getType(), "Clear");
    }

    @Test
    public void testGetCost() throws Exception {
        this.boba = new ClearBoba(UUID.randomUUID());
        this.boba = new Ice(this.boba);
        assertEquals(this.boba.getCost(), 13000);
    }

    @Test
    public void testGetId() throws Exception {
        UUID id = UUID.randomUUID();
        this.boba = new ClearBoba(id);
        this.boba = new Ice(this.boba);
        assertEquals(this.boba.getId(), id);
    }

    @Test
    public void testGetToppings() throws Exception {
        UUID id = UUID.randomUUID();
        this.boba = new ClearBoba(id);
        this.boba = new Ice(this.boba);
        this.boba = new BlackBubble(this.boba);
        this.boba = new BrownSugar(this.boba);
        this.boba = new Cheese(this.boba);
        assertEquals(this.boba.getToppings().contains("Ice"), true);
        assertEquals(this.boba.getToppings().contains("Black Bubble"), true);
        assertEquals(this.boba.getToppings().contains("Brown Sugar"), true);
        assertEquals(this.boba.getToppings().contains("Cheese"), true);
    }

    @Test
    public void testGetToppingsSize() throws Exception {
        UUID id = UUID.randomUUID();
        this.boba = new ClearBoba(id);
        assertEquals(this.boba.getToppings().size(), 0);
        this.boba = new Ice(this.boba);
        assertEquals(this.boba.getToppings().size(), 1);

    }

    @Test
    public void testGetDiscount() throws Exception {
        UUID id = UUID.randomUUID();
        this.boba = new ClearBoba(id);
        this.boba = new BlackBubble(this.boba);
        this.boba = new BrownSugar(this.boba);
        this.boba = new Ice(this.boba);
        assertEquals(2300, this.boba.getDiscountCost(0.9));
    }

    @Test
    public void testIsConcreteBoba(){
        UUID id = UUID.randomUUID();
        Decorator boba = new Ice(new ClearBoba(id));
        assertEquals(boba.isConcreteBoba(), true);
        boba = new BlackBubble(boba);
        assertEquals(boba.isConcreteBoba(), false);
    }
}
