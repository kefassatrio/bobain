package kelompok2.kece.Bobain.repository;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.ConcreteBoba.MilkBoba;
import kelompok2.kece.Bobain.model.Decorator.BlackBubble;
import kelompok2.kece.Bobain.model.Decorator.BrownSugar;
import kelompok2.kece.Bobain.model.Decorator.Cheese;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BobaRepositoryTest {
    private BobaRepository bobaRepository;

    @Before
    public void setUp(){
        bobaRepository = new BobaRepository();
    }

    @Test
    public void testGetAllBoba(){
        Boba boba = new ClearBoba(UUID.randomUUID());
        bobaRepository.save(boba);
        assertEquals(bobaRepository.getAllBoba().size(), 1);
    }
    @Test
    public void testSaveBoba(){
        Boba boba = new ClearBoba(UUID.randomUUID());
        Boba savedBoba = bobaRepository.save(boba);
        assertEquals(boba, savedBoba);
    }

    @Test
    public void testGetBobaById(){
        UUID id = UUID.randomUUID();
        Boba boba = new ClearBoba(id);
        bobaRepository.save(boba);
        assertEquals(bobaRepository.getBobaById(id), boba);
    }

    @Test
    public void testGetSelectedToppings(){
        assertThat(bobaRepository.getSelectedToppings()).isNotNull();
    }

    @Test
    public void testAddToppingIce(){
        bobaRepository.addTopping("ice");
        assertEquals(bobaRepository.getSelectedToppings().get("ice"), "Ice");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 3000);
    }

    @Test
    public void testAddToppingCheese(){
        bobaRepository.addTopping("cheese");
        assertEquals(bobaRepository.getSelectedToppings().get("cheese"), "Cheese");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 5000);
    }

    @Test
    public void testAddToppingBrownSugar(){
        bobaRepository.addTopping("brown-sugar");
        assertEquals(bobaRepository.getSelectedToppings().get("brown-sugar"), "Brown Sugar");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 5000);
    }

    @Test
    public void testAddToppingBlackBubble(){
        bobaRepository.addTopping("black-bubble");
        assertEquals(bobaRepository.getSelectedToppings().get("black-bubble"), "Black Bubble");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 5000);
    }

    @Test
    public void testRemoveToppingIce(){
        bobaRepository.addTopping("ice");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 3000);
        bobaRepository.removeTopping("ice");
        assertEquals(bobaRepository.getSelectedToppings().containsKey("ice"), false);
        assertEquals(bobaRepository.getSelectedToppingsCost(), 0);
    }

    @Test
    public void testRemoveToppingCheese(){
        bobaRepository.addTopping("cheese");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 5000);
        bobaRepository.removeTopping("cheese");
        assertEquals(bobaRepository.getSelectedToppings().containsKey("cheese"), false);
        assertEquals(bobaRepository.getSelectedToppingsCost(), 0);
    }

    @Test
    public void testRemoveToppingBrownSugar(){
        bobaRepository.addTopping("brown-sugar");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 5000);
        bobaRepository.removeTopping("brown-sugar");
        assertEquals(bobaRepository.getSelectedToppings().containsKey("brown-sugar"), false);
        assertEquals(bobaRepository.getSelectedToppingsCost(), 0);
    }

    @Test
    public void testRemoveToppingBlackBubble(){
        bobaRepository.addTopping("black-bubble");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 5000);
        bobaRepository.removeTopping("black-bubble");
        assertEquals(bobaRepository.getSelectedToppings().containsKey("black-bubble"), false);
        assertEquals(bobaRepository.getSelectedToppingsCost(), 0);
    }


    @Test
    public void testGetCurrentBobaDefaultIsClearBoba(){
        assertEquals(bobaRepository.getCurrentBoba().getDescription(), "Clear Boba");
    }

    @Test
    public void testSetCurrentBobaToClearBoba(){
        assertEquals(bobaRepository.setCurrentBoba("clear").getDescription(), "Clear Boba");
        assertEquals(bobaRepository.getCurrentBoba().getDescription(), "Clear Boba");
    }

    @Test
    public void testSetCurrentBobaToMilkBoba(){
        assertEquals(bobaRepository.setCurrentBoba("milk").getDescription(), "Milk Boba");
        assertEquals(bobaRepository.getCurrentBoba().getDescription(), "Milk Boba");
    }

    @Test
    public void testSetCurrentBobaToTeaBoba(){
        assertEquals(bobaRepository.setCurrentBoba("tea").getDescription(), "Tea Boba");
        bobaRepository.setCurrentBoba("tea");
        assertEquals(bobaRepository.getCurrentBoba().getDescription(), "Tea Boba");
    }


    @Test
    public void testSaveCurrentBoba(){
        bobaRepository.setCurrentBoba("clear");
        bobaRepository.addTopping("black-bubble");
        bobaRepository.addTopping("brown-sugar");
        bobaRepository.saveCurrentBoba(2);
        assertEquals(bobaRepository.getAllBoba().size(), 2);
        assertEquals(bobaRepository.getAllBoba().get(0).getType(), "Clear");
        assertEquals(bobaRepository.getAllBoba().get(0).getToppings().contains("Black Bubble"), true);
        assertEquals(bobaRepository.getAllBoba().get(0).getToppings().contains("Brown Sugar"), true);
        assertEquals(bobaRepository.getAllBoba().get(1).getType(), "Clear");
        assertEquals(bobaRepository.getAllBoba().get(1).getToppings().contains("Black Bubble"), true);
        assertEquals(bobaRepository.getAllBoba().get(1).getToppings().contains("Brown Sugar"), true);
    }

    @Test
    public void testClearRepo(){
        bobaRepository.setCurrentBoba("clear");
        bobaRepository.addTopping("black-bubble");
        bobaRepository.addTopping("brown-sugar");
        bobaRepository.saveCurrentBoba(2);
        bobaRepository.clearRepo();
        assertEquals(bobaRepository.getAllBoba().size(), 0);
        assertEquals(bobaRepository.getCurrentBoba().getDescription(), "Clear Boba");
        assertEquals(bobaRepository.getSelectedToppings().size(), 0);
    }

    @Test
    public void testAddSelectedToppingsCost(){
        bobaRepository.addSelectedToppingsCost("Brown Sugar");
        bobaRepository.addSelectedToppingsCost("Black Bubble");
        bobaRepository.addSelectedToppingsCost("Ice");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 13000);
    }

    @Test
    public void testRemoveSelectedToppingsCost(){
        bobaRepository.addSelectedToppingsCost("Brown Sugar");
        bobaRepository.addSelectedToppingsCost("Black Bubble");
        bobaRepository.addSelectedToppingsCost("Ice");
        bobaRepository.removeSelectedToppingsCost("Brown Sugar");
        bobaRepository.removeSelectedToppingsCost("Black Bubble");
        bobaRepository.removeSelectedToppingsCost("Ice");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 0);
    }

    @Test
    public void testGetCurrentBobaType(){
        assertEquals(bobaRepository.getCurrentBobaType(), "Clear");
    }

    @Test
    public void testGetSelectedToppingsCost(){
        bobaRepository.setCurrentBoba("clear");
        bobaRepository.addTopping("black-bubble");
        bobaRepository.addTopping("brown-sugar");
        bobaRepository.addTopping("ice");
        assertEquals(bobaRepository.getSelectedToppingsCost(), 13000);
    }

    @Test
    public void testFormattedString(){
        assertEquals(bobaRepository.formattedString("black-bubble"), "Black Bubble");
    }

    @Test
    public void testCapitalize(){
        assertEquals(bobaRepository.capitalize("brown"), "Brown");
    }

    @Test
    public void testGetAllToppings(){
        bobaRepository.save(new Cheese(new BrownSugar(new BlackBubble(new ClearBoba(UUID.randomUUID())))));
        assertEquals(bobaRepository.getAllToppings().get(2), "Cheese");
        assertEquals(bobaRepository.getAllToppings().get(1), "Brown Sugar");
        assertEquals(bobaRepository.getAllToppings().get(0), "Black Bubble");
    }

    @Test
    public void testGetDaftarToko(){
        bobaRepository.save(new Cheese(new BrownSugar(new BlackBubble(new ClearBoba(UUID.randomUUID())))));
        assertTrue(bobaRepository.getDaftarToko().contains("BobaIn Boba Store"));
        assertThat(bobaRepository.getDaftarToko()).isNotNull();
    }

    @Test
    public void testSaveComboBoba(){
        Boba milkBoba = new MilkBoba(UUID.randomUUID());
        bobaRepository.saveComboBoba(milkBoba);
        assertEquals(bobaRepository.getCurrentBobaType(), "Clear");
    }
}
