package kelompok2.kece.Bobain.controller;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.repository.BobaRepository;
import kelompok2.kece.Bobain.service.BobaServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = BobainController.class)
public class BobainControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BobaRepository bobaRepository;

    @MockBean
    private BobaServiceImpl bobaService;

    @Test
    public void testBobainHome() throws Exception {
        mockMvc.perform(get("/test"))
                .andExpect(model().attributeExists("currentBoba"))
                .andExpect(model().attributeExists("allBoba"))
                .andExpect(model().attributeExists("selectedToppings"))
                .andExpect(view().name("Bobain/home"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetRepo(){
        assertEquals(BobainController.getBobaRepository().getCurrentBobaType(), "Clear");
    }

    @Test
    public void testSaveCurrentBoba() throws Exception{
        mockMvc.perform(post("/api/save")
                .param("jumlah", String.valueOf(10)))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void testSetCurrentBobaToClearBoba() throws Exception {
        mockMvc.perform(post("/api/set-boba=clear"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));

    }

    @Test
    public void testSetCurrentBobaToMilkBoba() throws Exception{
        mockMvc.perform(post("/api/set-boba=milk"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void testSetCurrentBobaToTeaBoba() throws Exception{
        mockMvc.perform(post("/api/set-boba=tea"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void testAddToppingIce() throws Exception{
        mockMvc.perform(post("/api/add-topping=ice"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void testAddToppingCheese() throws Exception {
        mockMvc.perform(post("/api/add-topping=cheese"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void testAddToppingBrownSugar() throws Exception{
        mockMvc.perform(post("/api/add-topping=brown-sugar"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void addToppingBlackBubble() throws Exception {
        mockMvc.perform(post("/api/add-topping=black-bubble"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void testRemoveToppingIce() throws Exception {
        mockMvc.perform(post("/api/remove-topping=ice"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void testRemoveToppingCheese() throws Exception {
        mockMvc.perform(post("/api/remove-topping=cheese"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void testRemoveToppingBrownSugar() throws Exception {
        mockMvc.perform(post("/api/remove-topping=brown-sugar"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void testRemoveToppingBlackBubble() throws Exception {
        mockMvc.perform(post("/api/remove-topping=black-bubble"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/custom-boba"));
    }

    @Test
    public void testCustomBobaPage() throws Exception {
        mockMvc.perform(get("/custom-boba"))
                .andExpect(model().attributeExists("currentBoba"))
                .andExpect(model().attributeExists("allBoba"))
                .andExpect(model().attributeExists("selectedToppings"))
                .andExpect(model().attributeExists("selectedToppingsCost"))
                .andExpect(view().name("Bobain/CustomBobaPage"))
                .andExpect(status().isOk());
    }

    @Test
    public void testStoreCartPage() throws Exception {
        mockMvc.perform(post("/store-cart")
                .param("code", "sesuatu"))
                .andExpect(model().attributeExists("daftarToko"))
                .andExpect(model().attributeExists("bobaidcart"))
                .andExpect(view().name("Bobain/StoreCartPage"))
                .andExpect(status().isOk());
    }

    @Test
    public void testMainMenuPage() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(view().name("Bobain/MainMenuPage"))
                .andExpect(status().isOk());
    }

    @Test
    public void testSaveCheesyBubble() throws Exception{
        mockMvc.perform(post("/api/save-cheesy-bubble"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void testSaveIceCheeseTea() throws Exception{
        mockMvc.perform(post("/api/save-ice-cheese-tea"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void testSaveIceSweetMilk() throws Exception{
        mockMvc.perform(post("/api/save-ice-sweet-milk"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void testSaveIceBubbleTea() throws Exception{
        mockMvc.perform(post("/api/save-ice-bubble-tea"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

}
