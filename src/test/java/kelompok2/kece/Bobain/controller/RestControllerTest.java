package kelompok2.kece.Bobain.controller;

import kelompok2.kece.Bobain.receipt.service.CartServiceImpl;
import kelompok2.kece.Bobain.repository.BobaRepository;
import kelompok2.kece.Bobain.service.BobaServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = RestController.class)
public class RestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BobaRepository bobaRepository;

    @MockBean
    private BobaServiceImpl bobaService;

    @MockBean
    private CartServiceImpl cartService;

    @Test
    public void testComboIceCheeseTea() throws Exception {
        mockMvc.perform(post("/rest-api/save-ice-cheese-tea"))
                .andExpect(status().isOk());
    }

    @Test
    public void testComboIceSweetMilk() throws Exception {
        mockMvc.perform(post("/rest-api/save-ice-sweet-milk"))
                .andExpect(status().isOk());
    }

    @Test
    public void testComboIceBubbleTea() throws Exception {
        mockMvc.perform(post("/rest-api/save-ice-bubble-tea"))
                .andExpect(status().isOk());
    }
    @Test
    public void testComboCheesyBubble() throws Exception {
        mockMvc.perform(post("/rest-api/save-cheesy-bubble"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetFromCartService() throws Exception {
        mockMvc.perform(get("/rest-api/cart/get-orders"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/rest-api/cart/get-receipt"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/rest-api/cart/get-total-price"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/rest-api/cart/get-receipts"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteOrder() throws Exception {
        mockMvc.perform(post("/rest-api/save")
                .param("jumlah", String.valueOf(1)));
        mockMvc.perform(get("/cart"));
        mockMvc.perform(post("/rest-api/cart/delete")
                .param("index", String.valueOf(0)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDiscount() throws Exception {
        mockMvc.perform(post("/rest-api/cart/discount")
                .param("code", "TEST"))
                .andExpect(status().isOk());
    }

    @Test
    public void testClearOrder() throws Exception {
        mockMvc.perform(post("/rest-api/cart/clear-order"))
                .andExpect(status().isOk());
    }
    
    @Test
    public void testStoreCart() throws Exception{
        mockMvc.perform(get("/rest-api/store-cart-page"))
                .andExpect(status().isOk());
    }

    @Test
    public void testStoreReceipt() throws Exception {
        mockMvc.perform(post("/rest-api/cart/store-receipt"))
                .andExpect(status().isOk());
    }

    public void testBobaIdCart() throws Exception{
        mockMvc.perform(post("/rest-api/boba-id-cart"))
                .andExpect(status().isOk());
    }
}