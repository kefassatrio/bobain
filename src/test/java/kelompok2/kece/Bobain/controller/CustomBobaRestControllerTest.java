package kelompok2.kece.Bobain.controller;

import kelompok2.kece.Bobain.repository.BobaRepository;
import kelompok2.kece.Bobain.service.BobaServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = CustomBobaRestController.class)
public class CustomBobaRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BobaRepository bobaRepository;

    @MockBean
    private BobaServiceImpl bobaService;




    @Test
    public void testCurrentBoba() throws Exception {
        mockMvc.perform(get("/rest-api/current-boba"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAllBoba() throws Exception {
        mockMvc.perform(get("/rest-api/all-boba"))
                .andExpect(status().isOk());
    }

    @Test
    public void testSelectedToppings() throws Exception {
        mockMvc.perform(get("/rest-api/selected-toppings"))
                .andExpect(status().isOk());
    }

    @Test
    public void testSelectedToppingsCost() throws Exception {
        mockMvc.perform(get("/rest-api/selected-toppings-cost"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDescriptionFromSelectedToppings() throws Exception {
        mockMvc.perform(get("/rest-api/description-from-selected-toppings"))
                .andExpect(status().isOk());
    }


    @Test
    public void testSaveCurrentBoba() throws Exception {
        mockMvc.perform(post("/rest-api/save")
                .param("jumlah", String.valueOf(10)))
                .andExpect(status().isOk());
    }

    @Test
    public void testSaveSetBoba() throws Exception {
        mockMvc.perform(post("/rest-api/set-boba=clear"))
                .andExpect(status().isOk());

        mockMvc.perform(post("/rest-api/set-boba=tea"))
                .andExpect(status().isOk());

        mockMvc.perform(post("/rest-api/set-boba=milk"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddTopping() throws Exception {
        mockMvc.perform(post("/rest-api/add-topping=ice"))
                .andExpect(status().isOk());
        mockMvc.perform(post("/rest-api/add-topping=cheese"))
                .andExpect(status().isOk());
        mockMvc.perform(post("/rest-api/add-topping=brown-sugar"))
                .andExpect(status().isOk());
        mockMvc.perform(post("/rest-api/add-topping=black-bubble"))
                .andExpect(status().isOk());
    }

    @Test
    public void testRemoveTopping() throws Exception {
        mockMvc.perform(post("/rest-api/remove-topping=ice"))
                .andExpect(status().isOk());
        mockMvc.perform(post("/rest-api/remove-topping=cheese"))
                .andExpect(status().isOk());
        mockMvc.perform(post("/rest-api/remove-topping=brown-sugar"))
                .andExpect(status().isOk());
        mockMvc.perform(post("/rest-api/remove-topping=black-bubble"))
                .andExpect(status().isOk());
    }

}
