package kelompok2.kece.Bobain.receipt.core;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import kelompok2.kece.Bobain.model.ConcreteBoba.*;

import java.util.List;
import java.util.UUID;

public class ReceiptTest {
    @Test
    public void testAddRemoveOrder(){
        Receipt receipt = new ReceiptImpl();
        assertEquals(0, receipt.getOrders().size());

        Order test = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        receipt.addOrder(test);
        assertTrue(receipt.getOrders().contains(test));

        receipt.removeOrder(test);
        assertTrue(!receipt.getOrders().contains(test));
    }

    @Test
    public void testGetTotalPrice(){
        Receipt receipt = new ReceiptImpl();
        Order order1 = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Order order2 = new DrinkOrder(new MilkBoba(UUID.randomUUID()));
        Order order3 = new DrinkOrder(new TeaBoba(UUID.randomUUID()));

        receipt.addOrder(order1);
        receipt.addOrder(order2);
        receipt.addOrder(order3);

        int total = order1.getPrice() + order2.getPrice() + order3.getPrice();

        assertEquals(total, receipt.getTotalPrice());
    }

    @Test
    public void testDiscount(){
        Receipt receipt = new ReceiptImpl();
        Order order1 = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Order order2 = new DrinkOrder(new MilkBoba(UUID.randomUUID()));
        Order order3 = new DrinkOrder(new TeaBoba(UUID.randomUUID()));

        receipt.addOrder(order1);
        receipt.addOrder(order2);
        receipt.addOrder(order3);

        int total = order1.getPrice() + order2.getPrice() + order3.getPrice();
        receipt.discount(0.5);

        assertEquals(total*0.5, receipt.getTotalPrice());
    }

    @Test
    public void testGetSetID(){
        Receipt receipt = new ReceiptImpl();
        UUID id = UUID.randomUUID();
        receipt.setId(id);
        assertEquals(id, receipt.getId());
    }

    @Test
    public void testClearOrder(){
        Receipt receipt = new ReceiptImpl();
        Order order1 = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Order order2 = new DrinkOrder(new MilkBoba(UUID.randomUUID()));
        Order order3 = new DrinkOrder(new TeaBoba(UUID.randomUUID()));

        receipt.addOrder(order1);
        receipt.addOrder(order2);
        receipt.addOrder(order3);

        receipt.clearOrder();
        assertTrue(receipt.getOrders().isEmpty());
    }

    @Test
    public void testGetDescriptions(){
        Receipt receipt = new ReceiptImpl();
        Order order1 = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Order order2 = new DrinkOrder(new MilkBoba(UUID.randomUUID()));
        Order order3 = new DrinkOrder(new TeaBoba(UUID.randomUUID()));

        String ord1_dsc = order1.getDescription();
        String ord2_dsc = order2.getDescription();
        String ord3_dsc = order3.getDescription();

        receipt.addOrder(order1);
        receipt.addOrder(order2);
        receipt.addOrder(order3);

        List<String> descriptions = receipt.getDescriptions();

        assertEquals(ord1_dsc, descriptions.get(0));
        assertEquals(ord2_dsc, descriptions.get(1));
        assertEquals(ord3_dsc, descriptions.get(2));
    }

    @Test
    public void testGetPrices(){
        Receipt receipt = new ReceiptImpl();
        Order order1 = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Order order2 = new DrinkOrder(new MilkBoba(UUID.randomUUID()));
        Order order3 = new DrinkOrder(new TeaBoba(UUID.randomUUID()));

        int ord1_dsc = order1.getPrice();
        int ord2_dsc = order2.getPrice();
        int ord3_dsc = order3.getPrice();

        receipt.addOrder(order1);
        receipt.addOrder(order2);
        receipt.addOrder(order3);

        List<Integer> prices = receipt.getPrices();

        assertEquals(ord1_dsc, prices.get(0));
        assertEquals(ord2_dsc, prices.get(1));
        assertEquals(ord3_dsc, prices.get(2));
    }

    @Test
    public void testGetQuantity(){
        Receipt receipt = new ReceiptImpl();
        Order order1 = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Order order2 = new DrinkOrder(new MilkBoba(UUID.randomUUID()));
        Order order3 = new DrinkOrder(new TeaBoba(UUID.randomUUID()));

        receipt.addOrder(order1);
        receipt.addOrder(order2);
        receipt.addOrder(order3);

        assertEquals(3, receipt.getQuantity());
    }
}
