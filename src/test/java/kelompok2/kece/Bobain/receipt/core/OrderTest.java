package kelompok2.kece.Bobain.receipt.core;

import kelompok2.kece.Bobain.model.ConcreteBoba.TeaBoba;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.Boba;

import java.util.UUID;

public class OrderTest {
    @Test
    public void testGetSetDrink(){
        DrinkOrder order = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Boba boba = new TeaBoba(UUID.randomUUID());
        order.setDrink(boba);
        assertEquals(boba, order.getDrink());
    }

    @Test
    public void testGetPrice(){
        Order order = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        assertEquals(10000, order.getPrice());
    }

    @Test
    public void testSetPrice(){
        Order order = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        order.setPrice(10000);
        assertEquals(10000, order.getPrice());
    }

    @Test
    public void testDiscount(){
        Order order = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        int curPrice = order.getPrice();
        order.discount(0.5);
        assertEquals((int)curPrice*0.5, order.getPrice());
    }

    @Test
    public void testGetDescription(){
        Order order = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Boba boba = new ClearBoba(UUID.randomUUID());
        assertEquals(boba.getDescription(), order.getDescription());
    }
}
