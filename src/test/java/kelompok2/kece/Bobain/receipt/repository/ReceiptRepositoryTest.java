package kelompok2.kece.Bobain.receipt.repository;

import kelompok2.kece.Bobain.receipt.core.Receipt;
import kelompok2.kece.Bobain.receipt.core.ReceiptImpl;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class ReceiptRepositoryTest {
    @Test
    public void testAddReceipt(){
        ReceiptRepository repo = new ReceiptRepository();
        Receipt receipt = new ReceiptImpl();

        repo.addReceipt(receipt);
        assertTrue(repo.getReceiptMap().containsValue(receipt));
    }

    @Test
    public void testDeleteReceipt(){
        ReceiptRepository repo = new ReceiptRepository();
        Receipt receipt = new ReceiptImpl();

        repo.addReceipt(receipt);
        assertTrue(repo.getReceiptMap().containsValue(receipt));

        repo.deleteReceipt(receipt.getId());
        assertFalse(repo.getReceiptMap().containsValue(receipt));
    }

    @Test
    public void testGetReceipts(){
        ReceiptRepository repo = new ReceiptRepository();
        Receipt receipt1 = new ReceiptImpl();
        Receipt receipt2 = new ReceiptImpl();

        repo.addReceipt(receipt1);
        repo.addReceipt(receipt2);

        List<Receipt> receiptList = repo.getReceipts();
        assertTrue(receiptList.contains(receipt1));
        assertTrue(receiptList.contains(receipt2));
    }

    @Test
    public void testGetIdMap(){
        ReceiptRepository repo = new ReceiptRepository();
        Receipt receipt1 = new ReceiptImpl();
        Receipt receipt2 = new ReceiptImpl();

        repo.addReceipt(receipt1);
        repo.addReceipt(receipt2);

        Map<String, UUID> idMap = repo.getIdMap();
        assertEquals(receipt1.getId(), idMap.get(receipt1.getId().toString()));
        assertEquals(receipt2.getId(), idMap.get(receipt2.getId().toString()));
    }
}
