package kelompok2.kece.Bobain.receipt.service;

import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.ConcreteBoba.TeaBoba;
import kelompok2.kece.Bobain.receipt.core.DrinkOrder;
import kelompok2.kece.Bobain.receipt.core.Order;
import kelompok2.kece.Bobain.receipt.core.Receipt;
import kelompok2.kece.Bobain.receipt.repository.ReceiptRepository;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class CartTest {
    @Test
    public void testGetRepository(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);

        assertEquals(repo, service.getRepository());
    }

    @Test
    public void testGetReceipt(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);

        assertEquals(0, service.getReceipt().getQuantity());
    }

    @Test
    public void testStoreReceipt(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);

        Receipt receipt = service.getReceipt();
        service.storeReceipt();
        assertTrue(repo.getReceipts().contains(receipt));
    }

    @Test
    public void testGetOrders(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);
        assertTrue(service.getOrders().size() == 0);
    }

    @Test
    public void testGetBoba(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);
        service.clearOrder();

        Boba boba = new ClearBoba(UUID.randomUUID());
        Order order = new DrinkOrder(boba);
        service.addOrder(order);
        assertTrue(service.getOrders().contains(order));

        List<Boba> bobaList = service.getBoba();
        Boba boba2 = bobaList.get(0);
        assertEquals(boba, boba2);
    }

    @Test
    public void testClearOrders(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);

        Order order = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        service.addOrder(order);
        assertTrue(service.getOrders().size() == 1);

        service.clearOrder();
        assertTrue(service.getOrders().size() == 0);
    }

    @Test
    public void testAddRemoveOrder(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);
        service.clearOrder();

        Order order = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        service.addOrder(order);
        assertTrue(service.getOrders().contains(order));

        service.removeOrder(order);
        assertFalse(service.getOrders().contains(order));
    }

    @Test
    public void testGetOrder(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);
        service.clearOrder();

        Order order = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        service.addOrder(order);
        assertTrue(service.getOrders().contains(order));

        Order order2 = service.getOrder(0);
        assertEquals(order, order2);
    }

    @Test
    public void testRemoveOrderIndex(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);
        service.clearOrder();

        Order order = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        service.addOrder(order);
        assertTrue(service.getOrders().contains(order));

        service.removeOrder(0);
        assertFalse(service.getOrders().contains(order));
    }

    @Test
    public void testAddGetDiscount(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);

        String code = "TESTPROMO";
        double discount = 0.8;

        service.addDiscountCode(code, discount);
        assertEquals(discount, service.getDiscountCode(code));
    }

    @Test
    public void testDiscount(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);
        double discount = 0.8;
        service.clearOrder();

        Order order1 = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Order order2 = new DrinkOrder(new TeaBoba(UUID.randomUUID()));
        int order1_price = order1.getPrice();
        int order2_price = order2.getPrice();
        service.addOrder(order1);
        service.addOrder(order2);
        int total = service.getReceipt().getTotalPrice();

        service.discount(discount);
        int order1_newPrice = order1.getPrice();
        int order2_newPrice = order2.getPrice();
        assertEquals((int)order1_price*discount, order1_newPrice);
        assertEquals((int)order2_price*discount, order2_newPrice);
        assertEquals((int)total*discount, service.getReceipt().getTotalPrice());
    }

    @Test
    public void testVerifyDiscount(){
        ReceiptRepository repo = new ReceiptRepository();
        CartService service = new CartServiceImpl(repo);
        service.clearOrder();

        String code = "TESTPROMO";
        double discount = 0.8;
        service.addDiscountCode(code, discount);

        Order order1 = new DrinkOrder(new ClearBoba(UUID.randomUUID()));
        Order order2 = new DrinkOrder(new TeaBoba(UUID.randomUUID()));
        int order1_price = order1.getPrice();
        int order2_price = order2.getPrice();
        service.addOrder(order1);
        service.addOrder(order2);
        int total = service.getReceipt().getTotalPrice();

        service.verifyDiscount("TESTPROMO");
        int order1_newPrice = order1.getPrice();
        int order2_newPrice = order2.getPrice();
        assertEquals((int)order1_price*discount, order1_newPrice);
        assertEquals((int)order2_price*discount, order2_newPrice);
        assertEquals((int)total*discount, service.getReceipt().getTotalPrice());
    }
}
