package kelompok2.kece.Bobain.receipt.controller;

import kelompok2.kece.Bobain.controller.BobainController;
import kelompok2.kece.Bobain.model.Boba;
import kelompok2.kece.Bobain.model.ConcreteBoba.ClearBoba;
import kelompok2.kece.Bobain.model.Decorator.Ice;
import kelompok2.kece.Bobain.receipt.core.DrinkOrder;
import kelompok2.kece.Bobain.receipt.core.Order;
import kelompok2.kece.Bobain.receipt.core.Receipt;
import kelompok2.kece.Bobain.receipt.repository.ReceiptRepository;
import kelompok2.kece.Bobain.receipt.service.CartService;
import kelompok2.kece.Bobain.receipt.service.CartServiceImpl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = CartController.class)
public class CartControllerTest {
    @Autowired
    private MockMvc mockmvc;

    @MockBean
    private ReceiptRepository repository;

    @MockBean
    private CartServiceImpl service;

    @Test
    public void testGetService(){
        assertEquals(0, CartController.getService().getOrders().size());
    }

    @Test
    public void testCartPage() throws Exception{
        mockmvc.perform(get("/cart"))
                .andExpect(model().attributeExists("receipt"))
                .andExpect(model().attributeExists("orders"))
                .andExpect(view().name("Bobain/CartPage"))
                .andExpect(status().isOk());
    }

    @Test
    public void testHistoryPage() throws Exception{
        mockmvc.perform(get("/cart/history"))
                .andExpect(model().attributeExists("receipts"))
                .andExpect(view().name("Bobain/HistoryPage"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteOrder() throws Exception{
        String index = "0";
        mockmvc.perform(post("/cart/delete")
                .param("index", index))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/cart"));
    }

    @Test
    public void testDiscount() throws Exception{
        String value = "PROMO80";
        mockmvc.perform(post("/cart/discount")
                .param("discount", value))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/cart"));
    }

    @Test
    public void testStoreReceipt() throws Exception{
        mockmvc.perform(post("/cart/store-receipt"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void testClearOrder() throws Exception {
        mockmvc.perform(post("/cart/clear-order"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/cart"));
    }
}
